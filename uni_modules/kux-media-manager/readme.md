# kux-media-manager
一个获取相册媒体的媒体管理器插件，通过简洁的API即可快速获取相册媒体数据，方便灵活的对接任何UI展示相册文件。

## 插件特色
+ 简介的API设计
+ 支持获取视频，音频，最近照片视频。本地音乐等
+ 支持获取媒体文件元数据信息
+ 支持生成缩略图

### 基本用法
```ts
import { useMediaManager } from '@/uni_modules/kux-media-manager';

const mediaManager = useMediaManager();

// 获取所有相册媒体
console.log(mediaManager.loadAlbums());
// 获取相册分类名列表
console.log(mediaManager.getAlbumCategories());
// 获取指定分类的媒体数量
console.log(mediaManager.getMediaFileCountForAlbum('weixin'));
// 获取最近媒体数据
console.log(mediaManager.getRecentMedia());
// 获取指定分类下的媒体数据
console.log(mediaManager.getMediaFilesByAlbumName('Camera'));
// 获取本地视频
console.log(mediaManager.getLocalVideos());
// 获取本地音乐列表
console.log(mediaManager.getLocalMusicList());
```

> **注意**
> 
> 需要自定义基座才能正常使用该插件

## API
### isLoading
+ 说明：是否正在获取媒体资源
+ 类型：`isLoading(): boolean`

### getAlbumCategories
+ 说明：获取相册分类名列表
+ 类型：`getAlbumCategories(): string[]`

### getRecentMedia
+ 说明：获取最近媒体数据
+ 类型：`getRecentMedia(): KuxLocalMedia[]`

### loadAlbums
+ 说明：获取相册所有媒体
+ 类型：`loadAlbums(): KuxAlbum[]`

### getMediaFilesByAlbumName
+ 说明：获取指定分类下的媒体数据
+ 类型：`getMediaFilesByAlbumName(name: string): KuxLocalMedia[]`

### getLocalVideos
+ 说明：获取本地视频
+ 类型：`getLocalVideos(): KuxLocalVideo[]`

### generateVideoThumbnail
+ 说明：生成视频缩略图
+ 类型：`generateVideoThumbnail(videoPath: string, completion: (thumbPath: string | null) => void): void`

### generateVideoThumbnailSync
+ 说明：生成视频缩略图【同步版本】
+ 类型：`generateVideoThumbnailSync(videoPath: string): Promise<string | null>`

### getVideoDuration
+ 说明：获取视频时长
+ 类型：`getVideoDuration(videoPath: string, completion: (duration: number | null) => void): void`

### getVideoDurationSync
+ 说明：获取视频时长【同步版本】
+ 类型：`getVideoDurationSync(videoPath: string): Promise<number | null>`

### getLocalMusicList
+ 说明：获取本地音乐列表
+ 类型：`getLocalMusicList(): KuxLocalMusic[]`

### getMediaFileInfo
+ 说明：获取指定路径的媒体文件信息
+ 类型：`getMediaFileInfo(fileId: number, completion: (fileInfo: KuxMediaFileInfo) => void): void`

### getMediaFileInfoSync
+ 说明：获取指定路径的媒体文件信息【同步版本】
+ 类型：`getMediaFileInfoSync(fileId: number): Promise<KuxMediaFileInfo>`

### onError
+ 说明：捕获全局错误
+ 类型：`onError(callback: (error: IUniError) => void): void`

### KuxAlbum 说明
| 参数名 | 类型 | 说明
| --- | --- | ---
| name | `string` | 相册分类名
| medias | `KuxLocalMedia[]` | 媒体数据集

### KuxLocalMedia 说明
| 参数名 | 类型 | 说明
| --- | --- | ---
| id | `number` | 媒体编号
| path | `string` | 本地路径
| contentUri | `string?` | content地址，例如：`content://media/external/file/1000014710`
| mediaType | `number` | 媒体类型 1-图片 2-视频

### KuxLocalVideo 说明
| 参数名 | 类型 | 说明
| --- | --- | ---
| id | `number` | 媒体编号
| path | `string` | 本地路径
| contentUri | `string?` | content地址，例如：`content://media/external/file/1000014710`
| thumbPath | `string?` | 缩略图路径
| duration | `number?` | 视频时长

### KuxLocalMusic 说明
| 参数名 | 类型 | 说明
| --- | --- | ---
| id | `number` | 媒体编号
| path | `string` | 本地路径
| contentUri | `string?` | content地址，例如：`content://media/external/file/1000014710`
| title | `string` | 音乐标题
| artist | `string` | 艺术家信息
| album | `string` | 音乐专辑
| duration | `number` | 音乐时长

### KuxMediaFileInfo 说明
| 参数名 | 类型 | 说明
| --- | --- | ---
| id | `number?` | 媒体文件编号
| displayName | `string?` | 展示名称
| dateAdded | `number?` | 被添加到媒体库的日期和时间，时间戳格式
| dateModified | `number?` | 最后修改日期和时间，时间戳格式
| mimeType | `string?` | 文件的MIME类型
| size | `number?` | 文件的大小
| dateTaken | `number?` | 拍摄日期和时间，图片和视频类型文件有效，时间戳格式
| width | `number?` | 图片的宽度，图片类型文件有效
| height | `number?` | 图片的高度，图片类型文件有效
| orientation | `number?` | 需要旋转的角度，图片类型文件有效
| latitude | `number?` | 拍摄时的维度，图片类型文件有效
| longitude | `number?` | 拍摄时的经度，图片类型文件有效
| thumbPath | `string?` | 缩略图路径
| bucketDisplayName | `string?` | 所在的相册名称，图片和视频类型文件有效
| bucketId | `number?` | 所在的相册ID，图片和视频类型文件有效
| duration | `number?` | 时长，视频和音频文件有效
| resolution | `string?` | 视频的分辨率，视频文件有效
| description | `string?` | 描述信息，图片和视频文件有效
| artist | `string?` | 艺术家信息，视频和音频文件有效
| album | `string?` | 专辑信息，视频和音频文件有效
| albumArtist | `string?` | 专辑的艺术家信息，音频文件有效
| composer | `string?` | 作曲家，音频文件有效
| year | `number?` | 年份，音频文件有效
| track | `number?` | 专辑的曲目编号，音频文件有效
| isRingtone | `number?` | 是否被设置为铃声，音频文件有效，0-否 1-是
| isMusic | `number?` | 是否是音乐，音频文件有效，0-否 1-是
| isAlarm | `number?` | 是否被设置为闹钟铃声，音频文件有效，0-否 1-是
| isNotification | `number?` | 是否被设置为通知铃声，音频文件有效，0-否 1-是

---

### 结语
#### kux 不生产代码，只做代码的搬运工，致力于提供uts 的 js 生态轮子实现，欢迎各位大佬在插件市场搜索使用 kux 生态插件：[https://ext.dcloud.net.cn/search?q=kux](https://ext.dcloud.net.cn/search?q=kux)

### 友情推荐
+ [TMUI4.0](https://ext.dcloud.net.cn/plugin?id=16369)：包含了核心的uts插件基类.和uvue组件库
+ [UxFrame 低代码高性能UI框架](https://ext.dcloud.net.cn/plugin?id=16148)：低代码高性能UI框架
+ [GVIM即时通讯模版](https://ext.dcloud.net.cn/plugin?id=16419)：GVIM即时通讯模版，基于uni-app x开发的一款即时通讯模版
+ [t-uvue-ui](https://ext.dcloud.net.cn/plugin?id=15571)：T-UVUE-UI是基于UNI-APP X开发的前端UI框架
+ [uXui](https://ext.dcloud.net.cn/plugin?id=15726): graceUI作者的免费开源组件库
+ [wx-ui 基于uni-app x开发的高性能混合UI库](https://ext.dcloud.net.cn/plugin?id=15579)：基于uni-app x开发的高性能混合UI库，集成 uts api 和 uts component，提供了一套完整、高效且易于使用的UI组件和API，让您以更少的时间成本，轻松完成高性能应用开发。
+ [firstui-uvue](https://ext.dcloud.net.cn/plugin?id=16294)：FirstUI（unix）组件库，一款适配 uni-app x 的轻量、简洁、高效、全面的移动端组件库。
+ [easyXUI 不仅仅是UI 更是为UniApp X设计的电商模板库](https://ext.dcloud.net.cn/plugin?id=15602)：easyX 不仅仅是UI库，更是一个轻量、可定制的UniAPP X电商业务模板库，可作为官方组件库的补充,始终坚持简单好用、易上手