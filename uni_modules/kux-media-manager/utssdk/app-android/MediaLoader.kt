package uts.sdk.modules.kuxMediaManager

import android.content.ContentResolver
import android.content.Context
import android.database.Cursor
import android.net.Uri
import android.provider.MediaStore
import android.os.Build
import java.util.concurrent.atomic.AtomicBoolean
import io.dcloud.uts.UTSAndroid
import io.dcloud.uts.console
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import android.graphics.Bitmap
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever
import java.util.UUID
import kotlinx.coroutines.*
import android.content.*
import android.media.ThumbnailUtils
import androidx.exifinterface.media.ExifInterface

import android.graphics.drawable.Drawable
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import kotlinx.coroutines.suspendCancellableCoroutine
import kotlin.coroutines.resumeWithException
// import kotlinx.coroutines.withContext

data class LocalMedia(
	val id: Long,
	val path: String,
	val mediaType: Int, // MEDIA_TYPE_IMAGE 或 MEDIA_TYPE_VIDEO
	val contentUri: String?
)

data class Album(
    val name: String,
    val medias: List<LocalMedia>
)

data class LocalVideo(
	val id: Long,
	val path: String,
	val thumbPath: String?,
	val duration: Long?,
	val contentUri: String?
)

data class LocalMusic(
	val id: Long,
	val title: String,
	val artist: String,
	val album: String,
	val path: String,
	val duration: Long,
	val contentUri: String?
)

public class KuxMediaLoader(private val context: Context) {
	private val isLoading = AtomicBoolean(false)
	
	fun debug(content: String) {
		console.log(content)
	}
	
	fun isLoadingData(): Boolean {
		return isLoading.get()
	}
	
	// 设置加载数据的状态
	private fun setLoadingData(isLoading: Boolean) {
		this.isLoading.set(isLoading)
	}
	
	fun getMediaFileInfoSync(fileId: Long): Map<String, Any?> {
		try {
		    val contentResolver: ContentResolver = context.contentResolver
		        ?: return emptyMap<String, Any?>()
			
		    val projection = arrayOf(
		        MediaStore.MediaColumns._ID,
		        MediaStore.MediaColumns.DISPLAY_NAME,
		        MediaStore.MediaColumns.TITLE,
		        MediaStore.MediaColumns.DATE_ADDED,
		        MediaStore.MediaColumns.DATE_MODIFIED,
		        MediaStore.MediaColumns.MIME_TYPE,
		        MediaStore.MediaColumns.SIZE,
				MediaStore.MediaColumns.DATE_TAKEN,
				MediaStore.MediaColumns.WIDTH,
				MediaStore.MediaColumns.HEIGHT
		    )
			
		    val fileInfo = mutableMapOf<String, Any?>()
			
			// val contentUri = getContentUriFromFilePath(filePath)
			val contentUri = MediaStore.Files.getContentUri("external")
			val selection = "${MediaStore.Files.FileColumns._ID} = ?"
			val selectionArgs = arrayOf(fileId.toString())
			
		    contentResolver.query(contentUri, projection, selection, selectionArgs, null)?.use { cursor ->
		        if (cursor.moveToFirst()) {
		            // 缓存列索引
		            val idIndex = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns._ID)
		            val nameIndex = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DISPLAY_NAME)
		            val titleIndex = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.TITLE)
		            val dateAddedIndex = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATE_ADDED)
		            val dateModifiedIndex = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATE_MODIFIED)
		            val mimeTypeIndex = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.MIME_TYPE)
		            val sizeIndex = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.SIZE)
			
		            val mimeType = cursor.getString(mimeTypeIndex) ?: ""
			
		            fileInfo.apply {
		                put("ID", cursor.getLong(idIndex))
		                put("DISPLAY_NAME", cursor.getString(nameIndex))
		                put("TITLE", cursor.getString(titleIndex))
		                put("DATE_ADDED", cursor.getLong(dateAddedIndex))
		                put("DATE_MODIFIED", cursor.getLong(dateModifiedIndex))
		                put("MIME_TYPE", mimeType)
		                put("SIZE", cursor.getLong(sizeIndex))
		            }
					
					// console.log(fileInfo)
			
		            // 根据 MIME 类型处理
		            when {
		                mimeType.startsWith("image/") -> processImageFileInfo(cursor, fileId, fileInfo)
		                mimeType.startsWith("video/") -> processVideoFileInfo(cursor, fileInfo)
		                mimeType.startsWith("audio/") -> processAudioFileInfo(cursor, fileInfo)
		                else -> {
		                    // 其他文件类型的额外处理
		                }
		            }
		        }
		    }
			
		    return fileInfo
		} catch (e: IllegalArgumentException) {
		    e.printStackTrace()
		    return emptyMap()
		} catch (e: Exception) {
		    e.printStackTrace()
		    return emptyMap()
		}
	}
	
	// 获取媒体文件信息
	fun getMediaFileInfo(fileId: Long, completion: (Map<String, Any?>) -> Unit) {
		val deferred = CompletableDeferred<Map<String, Any?>>()
		
		CoroutineScope(Dispatchers.IO).launch {
			try {
				val fileInfo = getMediaFileInfoWithContext(fileId)
				deferred.complete(fileInfo)
				withContext(Dispatchers.Main) {
					completion(fileInfo)
				}
			} catch (e: Exception) {
				deferred.completeExceptionally(e)
			}
		}
	}
	
	@Suppress("DEPRECATION")
	private fun processImageFileInfo(cursor: Cursor, fileId: Long, fileInfo: MutableMap<String, Any?>) {
		if (cursor.moveToFirst()) {
		    // 获取列索引并缓存，避免重复调用
		    val columnIndexMap = mapOf(
		        "DATE_TAKEN" to MediaStore.Images.Media.DATE_TAKEN,
		        "WIDTH" to MediaStore.Images.Media.WIDTH,
		        "HEIGHT" to MediaStore.Images.Media.HEIGHT,
		        "ORIENTATION" to null,  // 单独处理，需调用 Exif
		        "DESCRIPTION" to null,  // 为空，直接设置 null
		        "LATITUDE" to null,     // 单独处理，通过 EXIF 获取
		        "LONGITUDE" to null,    // 单独处理，通过 EXIF 获取
		        "BUCKET_DISPLAY_NAME" to MediaStore.Images.Media.BUCKET_DISPLAY_NAME,
		        "BUCKET_ID" to MediaStore.Images.Media.BUCKET_ID,
				"ID" to MediaStore.Images.Media._ID
		    )
			
		    // 读取 EXIF 信息
		    val orientation = getOrientationFromExif(fileId)
		    val (latitude, longitude) = getLatLongFromExif(fileId)
			
		    // 获取 ID
		    val id = cursor.getLong(cursor.getColumnIndexOrThrow(MediaStore.Images.Media._ID))
			
		    // 遍历映射表并将字段添加到 fileInfo
		    columnIndexMap.forEach { (key, columnName) ->
		        val columnIndex = if (columnName != null) cursor.getColumnIndex(columnName) else -1
		        if (columnIndex != -1) {
		            when (key) {
		                "DATE_TAKEN", "LATITUDE", "LONGITUDE", "ID" -> fileInfo[key] = cursor.getLong(columnIndex)  // 长整型
		                "WIDTH", "HEIGHT" -> fileInfo[key] = cursor.getInt(columnIndex)  // 整型
		                else -> fileInfo[key] = cursor.getString(columnIndex)  // 字符串类型
		            }
		        } else {
		            // 特殊字段的处理
		            when (key) {
		                "ORIENTATION" -> fileInfo[key] = orientation
		                "LATITUDE" -> fileInfo[key] = latitude
		                "LONGITUDE" -> fileInfo[key] = longitude
		                "DESCRIPTION" -> fileInfo[key] = null  // 如果没有描述，直接设为 null
		            }
		        }
		    }
		
			// 获取缩略图
			val uri = ContentUris.withAppendedId(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, id)
			val inputStream = context.contentResolver.openInputStream(uri)
			val bitmap = ThumbnailUtils.extractThumbnail(BitmapFactory.decodeStream(inputStream), 100, 100)
			inputStream?.close()
			
		    // 缩略图路径
		    fileInfo["THUMB_PATH"] = bitmap?.let { saveBitmapToCache(bitmap, id) }
		}
	}
	
	@Suppress("DEPRECATION")
	private fun processVideoFileInfo(cursor: Cursor, fileInfo: MutableMap<String, Any?>) {
	    if (cursor.moveToFirst()) {
	        // 创建字段和列名的映射
	        val columnIndexMap = mapOf(
	            "DATE_TAKEN" to MediaStore.Video.Media.DATE_TAKEN,
	            "DURATION" to MediaStore.Video.Media.DURATION,
	            "DESCRIPTION" to null,  // 这里暂时不取数据，直接赋 null
	            "BUCKET_DISPLAY_NAME" to MediaStore.Images.Media.BUCKET_DISPLAY_NAME, // 因为 Video 和 Images 共用这个字段
	            "BUCKET_ID" to MediaStore.Video.Media.BUCKET_ID,
	            "RESOLUTION" to MediaStore.Video.Media.RESOLUTION,
	            "ARTIST" to MediaStore.Video.Media.ARTIST,
	            "ALBUM" to MediaStore.Video.Media.ALBUM,
				"ID" to MediaStore.Video.Media._ID
	        )
			
			// 获取 ID
			val id = cursor.getLong(cursor.getColumnIndexOrThrow(MediaStore.Video.Media._ID))
	
	        // 遍历映射并将字段添加到 fileInfo
	        columnIndexMap.forEach { (key, columnName) ->
	            val columnIndex = if (columnName != null) cursor.getColumnIndex(columnName) else -1
	            if (columnIndex != -1) {  // 检查列是否存在
	                when (key) {
	                    "DATE_TAKEN", "DURATION", "ID" -> fileInfo[key] = cursor.getLong(columnIndex)  // 长整型
	                    else -> fileInfo[key] = cursor.getString(columnIndex)  // 字符串类型
	                }
	            } else if (key == "DESCRIPTION") {
	                fileInfo[key] = null // 特殊处理，若没有数据，直接赋值为 null
	            }
	        }
			
			// 获取缩略图
			val uri = ContentUris.withAppendedId(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, id)
			val inputStream = context.contentResolver.openInputStream(uri)
			val bitmap = ThumbnailUtils.extractThumbnail(BitmapFactory.decodeStream(inputStream), 100, 100)
			inputStream?.close()
			
			// 缩略图路径
			fileInfo["THUMB_PATH"] = bitmap?.let { saveBitmapToCache(bitmap, id) }
	    }
	}
	
	@Suppress("DEPRECATION")
	private fun processAudioFileInfo(cursor: Cursor, fileInfo: MutableMap<String, Any?>) {
	    // 检查 cursor 是否有效且移动到第一行
	    if (cursor.moveToFirst()) {
	        val columnIndexMap = mapOf(
	            "ARTIST" to MediaStore.Audio.Media.ARTIST,
	            "ALBUM" to MediaStore.Audio.Media.ALBUM,
	            "ALBUM_ARTIST" to MediaStore.Audio.Media.ALBUM_ARTIST,
	            "COMPOSER" to MediaStore.Audio.Media.COMPOSER,
	            "YEAR" to MediaStore.Audio.Media.YEAR,
	            "TRACK" to MediaStore.Audio.Media.TRACK,
	            "DURATION" to MediaStore.Audio.Media.DURATION,
	            "IS_RINGTONE" to MediaStore.Audio.Media.IS_RINGTONE,
	            "IS_MUSIC" to MediaStore.Audio.Media.IS_MUSIC,
	            "IS_ALARM" to MediaStore.Audio.Media.IS_ALARM,
	            "IS_NOTIFICATION" to MediaStore.Audio.Media.IS_NOTIFICATION
	        )
	
	        // 遍历字段并添加到 fileInfo
	        columnIndexMap.forEach { (key, columnName) ->
	            val columnIndex = cursor.getColumnIndex(columnName)
	            if (columnIndex != -1) {  // 检查列是否存在
	                when (key) {
	                    "YEAR", "TRACK" -> fileInfo[key] = cursor.getInt(columnIndex) // 整数类型
	                    "DURATION", "IS_RINGTONE", "IS_MUSIC", "IS_ALARM", "IS_NOTIFICATION" -> 
	                        fileInfo[key] = cursor.getLong(columnIndex) // 长整型
	                    else -> fileInfo[key] = cursor.getString(columnIndex) // 字符串类型
	                }
	            }
	        }
	    }
	}
	
	private fun getContentUriFromFilePath(filePath: String): Uri? {
	    val file = File(filePath)
	    val projection = arrayOf(MediaStore.Images.Media._ID)
	    val selection = MediaStore.Images.Media.DATA + " = ?"
	    val selectionArgs = arrayOf(file.absolutePath)
	
	    context.contentResolver.query(
	        MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
	        projection,
	        selection,
	        selectionArgs,
	        null
	    )?.use { cursor ->
	        if (cursor.moveToFirst()) {
	            val id = cursor.getLong(cursor.getColumnIndexOrThrow(MediaStore.Images.Media._ID))
	            return ContentUris.withAppendedId(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, id)
	        }
	    }
	    return null
	}
	
	private fun getOrientationFromExif(fileId: Long): Int {
	    val contentResolver: ContentResolver = context.contentResolver
	    val uri: Uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI.buildUpon().appendPath(fileId.toString()).build()
	
	    return try {
	        // 通过文件 ID 获取输入流
	        contentResolver.openInputStream(uri)?.use { inputStream ->
	            // 使用输入流创建 ExifInterface 实例
	            val exif = ExifInterface(inputStream)
	            // 读取方向信息
	            when (exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL)) {
	                ExifInterface.ORIENTATION_ROTATE_90 -> 90
	                ExifInterface.ORIENTATION_ROTATE_180 -> 180
	                ExifInterface.ORIENTATION_ROTATE_270 -> 270
	                else -> 0
	            }
	        } ?: 0 // 如果输入流为空，则返回 0
	    } catch (e: Exception) {
	        e.printStackTrace()
	        0
	    }
	}
	
	private fun getLatLongFromExif(fileId: Long): Pair<Double?, Double?> {
	    val contentResolver: ContentResolver = context.contentResolver
	    val uri: Uri = ContentUris.withAppendedId(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, fileId)
	
	    return try {
	        // 通过文件 ID 获取输入流
	        val inputStream = contentResolver.openInputStream(uri)
	        // 使用输入流创建 ExifInterface 实例
	        inputStream?.use { stream ->
	            val exif = ExifInterface(stream)
	            // 读取经纬度信息
	            val latLong = exif.latLong
	            Pair(latLong?.getOrNull(0), latLong?.getOrNull(1))
	        } ?: Pair(null, null) // 如果输入流为空，则返回 Pair(null, null)
	    } catch (e: Exception) {
	        e.printStackTrace()
	        Pair(null, null)
	    }
	}
	
	private suspend fun getMediaFileInfoWithContext(fileId: Long): Map<String, Any?> {
	    return withContext(Dispatchers.IO) {
	        try {
	            val contentResolver: ContentResolver = context.contentResolver
	                ?: return@withContext emptyMap<String, Any?>()
	
	            val projection = arrayOf(
	                MediaStore.MediaColumns._ID,
	                MediaStore.MediaColumns.DISPLAY_NAME,
	                MediaStore.MediaColumns.TITLE,
	                MediaStore.MediaColumns.DATE_ADDED,
	                MediaStore.MediaColumns.DATE_MODIFIED,
	                MediaStore.MediaColumns.MIME_TYPE,
	                MediaStore.MediaColumns.SIZE,
					MediaStore.MediaColumns.DATE_TAKEN,
					MediaStore.MediaColumns.WIDTH,
					MediaStore.MediaColumns.HEIGHT
	            )
	
	            val fileInfo = mutableMapOf<String, Any?>()
				
				// val contentUri = getContentUriFromFilePath(filePath)
				val contentUri = MediaStore.Files.getContentUri("external")
				val selection = "${MediaStore.Files.FileColumns._ID} = ?"
				val selectionArgs = arrayOf(fileId.toString())
	
	            contentResolver.query(contentUri, projection, selection, selectionArgs, null)?.use { cursor ->
	                if (cursor.moveToFirst()) {
	                    // 缓存列索引
	                    val idIndex = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns._ID)
	                    val nameIndex = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DISPLAY_NAME)
	                    val titleIndex = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.TITLE)
	                    val dateAddedIndex = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATE_ADDED)
	                    val dateModifiedIndex = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATE_MODIFIED)
	                    val mimeTypeIndex = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.MIME_TYPE)
	                    val sizeIndex = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.SIZE)
	
	                    val mimeType = cursor.getString(mimeTypeIndex) ?: ""
	
	                    fileInfo.apply {
	                        put("ID", cursor.getLong(idIndex))
	                        put("DISPLAY_NAME", cursor.getString(nameIndex))
	                        put("TITLE", cursor.getString(titleIndex))
	                        put("DATE_ADDED", cursor.getLong(dateAddedIndex))
	                        put("DATE_MODIFIED", cursor.getLong(dateModifiedIndex))
	                        put("MIME_TYPE", mimeType)
	                        put("SIZE", cursor.getLong(sizeIndex))
	                    }
						
						// console.log(fileInfo)
	
	                    // 根据 MIME 类型处理
	                    when {
	                        mimeType.startsWith("image/") -> processImageFileInfo(cursor, fileId, fileInfo)
	                        mimeType.startsWith("video/") -> processVideoFileInfo(cursor, fileInfo)
	                        mimeType.startsWith("audio/") -> processAudioFileInfo(cursor, fileInfo)
	                        else -> {
	                            // 其他文件类型的额外处理
	                        }
	                    }
	                }
	            }
	
	            fileInfo
	        } catch (e: IllegalArgumentException) {
	            e.printStackTrace()
				console.log(e)
	            emptyMap()
	        } catch (e: Exception) {
	            e.printStackTrace()
	            emptyMap()
	        }
	    }
	}
	
	// 获取相册分类名称
	fun getAlbumCategories(): List<String> {
		setLoadingData(true)
		try {
			val projection = arrayOf(
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
					MediaStore.Images.Media.BUCKET_DISPLAY_NAME
				} else {
					MediaStore.Images.Media.BUCKET_ID
				}
			)
			
			val selection = "${MediaStore.Files.FileColumns.MEDIA_TYPE} = ? OR " +
			                "${MediaStore.Files.FileColumns.MEDIA_TYPE} = ?"
			
			val selectionArgs = arrayOf(
				MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE.toString(),
				MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO.toString()
			)
			
			val uri: Uri = MediaStore.Files.getContentUri("external")
			
			val cursor: Cursor? = context.contentResolver.query(
				uri,
				projection,
				selection,
				selectionArgs,
				null
			)
			
			val albumCategories = mutableSetOf<String>()
			
			cursor?.use {
				val bucketDisplayNameIndex = it.getColumnIndexOrThrow(
					if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
						MediaStore.Images.Media.BUCKET_DISPLAY_NAME
					} else {
						MediaStore.Images.Media.BUCKET_ID
					}
				)
			
				while (it.moveToNext()) {
					val albumName = it.getString(bucketDisplayNameIndex)
					albumCategories.add(albumName)
				}
			}
			
			return albumCategories.toList()
		} finally {
			setLoadingData(false)
		}
	}
	
	// 获取某个相册分类名的媒体数量
	fun getMediaFileCountForAlbum(albumName: String): Int {
	    var fileCount = 0
	
	    val projection = arrayOf(
	        MediaStore.Files.FileColumns._ID // 只需要文件 ID
	    )
	
	    val selection = "${MediaStore.Images.Media.BUCKET_DISPLAY_NAME} = ? AND (" +
	            "${MediaStore.Files.FileColumns.MEDIA_TYPE} = ? OR " +
	            "${MediaStore.Files.FileColumns.MEDIA_TYPE} = ?)"
	
	    val selectionArgs = arrayOf(
	        albumName,
	        MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE.toString(),
	        MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO.toString()
	    )
	
	    val uri: Uri = MediaStore.Files.getContentUri("external")
	
	    try {
	        val cursor: Cursor? = context.contentResolver.query(
	            uri,
	            projection,
	            selection,
	            selectionArgs,
	            null // 不需要排序
	        )
	
	        cursor?.use {
	            // 文件总数等于查询到的行数
	            fileCount = it.count
	        }
	    } catch (e: Exception) {
	        e.printStackTrace()
	    }
	
	    return fileCount
	}
	
	// 模拟实现获取最近媒体数据
	fun getRecentMedia(): List<LocalMedia> {
	    setLoadingData(true)
	    try {
	        val projection = arrayOf(
	            MediaStore.Files.FileColumns._ID,
	            MediaStore.Files.FileColumns.DATA,
	            MediaStore.Files.FileColumns.MEDIA_TYPE
	        )
	        val selection = "${MediaStore.Files.FileColumns.MEDIA_TYPE} = ? OR " +
	                        "${MediaStore.Files.FileColumns.MEDIA_TYPE} = ?"
	        val selectionArgs = arrayOf(
	            MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE.toString(),
	            MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO.toString()
	        )
	        val sortOrder = "${MediaStore.Files.FileColumns.DATE_ADDED} DESC"
	
	        val uri: Uri = MediaStore.Files.getContentUri("external")
	
	        val cursor: Cursor? = context.contentResolver.query(
	            uri,
	            projection,
	            selection,
	            selectionArgs,
	            sortOrder
	        )
	        val mediaList = mutableListOf<LocalMedia>()
	        cursor?.use {
	            val idColumn = it.getColumnIndexOrThrow(MediaStore.Files.FileColumns._ID)
	            val dataColumn = it.getColumnIndexOrThrow(MediaStore.Files.FileColumns.DATA)
	            val mediaTypeColumn = it.getColumnIndexOrThrow(MediaStore.Files.FileColumns.MEDIA_TYPE)
	
	            while (it.moveToNext()) {
	                val id = it.getLong(idColumn)
	                val dataPath = it.getString(dataColumn)
	                val mediaType = it.getInt(mediaTypeColumn) // 媒体类型：图片或视频
					val contentUri = ContentUris.withAppendedId(MediaStore.Files.getContentUri("external"), id).toString()
	                mediaList.add(LocalMedia(id, dataPath, mediaType, contentUri))
	            }
	        }
	        return mediaList
	    } finally {
	        setLoadingData(false)
	    }
	}
	
	fun localVideos(completion: (List<LocalVideo>) -> Unit) {
		val deferred = CompletableDeferred<List<LocalVideo>>()
		    
		CoroutineScope(Dispatchers.IO).launch {
			try {
				val videos = getLocalVideosSync()
				deferred.complete(videos)
				withContext(Dispatchers.Main) {
					completion(videos)
				}
			} catch (e: Exception) {
				withContext(Dispatchers.Main) {
					completion(emptyList())
				}
			}
		}
	}
	
	fun getVideoThumbPath(videoPath: String, completion: (String?) -> Unit) {
		val deferred = CompletableDeferred<String?>()
		
		CoroutineScope(Dispatchers.IO).launch {
			try {
				val thumbPath = generateThumbnailForVideo(Uri.parse(videoPath))
				deferred.complete(thumbPath)
				withContext(Dispatchers.Main) {
					completion(thumbPath)
				}
			} catch (e: Exception) {
				withContext(Dispatchers.Main) {
					completion(null)
				}
			}
		}
	}
	
	fun getLocalVideos(): List<LocalVideo> {
		setLoadingData(true)
		try {
			val projection = arrayOf(MediaStore.Video.Media._ID, MediaStore.Video.Media.DATA)
			val sortOrder = "${MediaStore.Video.Media.DATE_ADDED} DESC"
			
			val cursor: Cursor? = context.contentResolver.query(
				MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
				projection,
				null,
				null,
				sortOrder
			)
			val videos = mutableListOf<LocalVideo>()
			cursor?.use {
				val idColumn = it.getColumnIndexOrThrow(MediaStore.Video.Media._ID)
				// val titleColumn = it.getColumnIndexOrThrow(MediaStore.Video.Media.TITLE)
				val dataColumn = it.getColumnIndexOrThrow(MediaStore.Video.Media.DATA)
				
				while (it.moveToNext()) {
					val id = it.getLong(idColumn)
					val dataPath = it.getString(dataColumn)
					val contentUri = ContentUris.withAppendedId(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, id).toString()
					// val title = it.getString(titleColumn)
					// val thumbPath = generateThumbnailForVideo(Uri.parse(dataPath))
					videos.add(LocalVideo(id, dataPath, null, null, contentUri))
				}
			}
			return videos
		} finally {
			setLoadingData(false)
		}
	}
	
	// 获取本地视频
	suspend fun getLocalVideosSync(): List<LocalVideo> {
		setLoadingData(true)
		return withContext(Dispatchers.IO) {
			val projection = arrayOf(MediaStore.Video.Media._ID, MediaStore.Video.Media.DATA)
			val sortOrder = "${MediaStore.Video.Media.DATE_ADDED} DESC"
	
			val cursor: Cursor? = context.contentResolver.query(
				MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
				projection,
				null,
				null,
				sortOrder
			)
			val videos = mutableListOf<LocalVideo>()
			cursor?.use {
				val idColumn = it.getColumnIndexOrThrow(MediaStore.Video.Media._ID)
				val dataColumn = it.getColumnIndexOrThrow(MediaStore.Video.Media.DATA)
	
				while (it.moveToNext()) {
					val id = it.getLong(idColumn)
					val dataPath = it.getString(dataColumn)
					val contentUri = ContentUris.withAppendedId(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, id).toString()
					// val duration = getVideoDuration(dataPath)
					videos.add(LocalVideo(id, dataPath, null, null, contentUri))
				}
			}
			setLoadingData(false)
			videos
		}
	}
	
	// 获取视频缩略图
	@Suppress("DEPRECATION")
	fun getThumbnailPathForVideo(videoId: Long): String? {
		// var thumbnailUri: Uri? = null
		val projection = arrayOf(MediaStore.Video.Thumbnails.DATA)
		val cursor: Cursor? = context.contentResolver.query(
			MediaStore.Video.Thumbnails.EXTERNAL_CONTENT_URI,
			projection,
			MediaStore.Video.Thumbnails.VIDEO_ID + "=?",
			arrayOf(videoId.toString()),
			null
		)
		cursor?.use {
			if (it.moveToFirst()) {
				val thumbnailColumnIndex = it.getColumnIndex(MediaStore.Video.Thumbnails.DATA)
				return it.getString(thumbnailColumnIndex)
			}
		}
		
		// 主动生成缩略图
		val thumbnail = MediaStore.Video.Thumbnails.getThumbnail(
			context.contentResolver,
			videoId,
			MediaStore.Video.Thumbnails.MINI_KIND,
			null
		)
		return thumbnail?.let { saveBitmapToCache(it, videoId) }
	}
	
	private fun saveBitmapToCache(bitmap: Bitmap, videoId: Long): String {
		val cacheDir = context.cacheDir
		val thumbnailFile = File(cacheDir, "thumbnail_$videoId.jpg")
		
		// 判断缓存是否存在
		if (thumbnailFile.exists()) {
			return thumbnailFile.absolutePath
		}
		
		FileOutputStream(thumbnailFile).use { fos ->
			bitmap.compress(Bitmap.CompressFormat.JPEG, 90, fos)
		}
		
		return thumbnailFile.absolutePath
	}
	
	// 初始化视频缩略图
	fun generateVideoThumbnail(
	    videoPath: String,
	    onThumbnailGenerated: (String?) -> Unit // 回调返回生成的路径
	) {
		val videoUri = Uri.parse("file://${videoPath}")
	    Glide.with(context)
	        .asBitmap()
	        .load(videoUri)
	        .frame(0) // 获取第一帧（单位：微秒）
	        .into(object : CustomTarget<Bitmap>() {
	            override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
	                // 保存 Bitmap 到文件
	                val cacheDir = context.cacheDir
	                val thumbnailFile = File(cacheDir, "thumbnail_${videoUri.lastPathSegment.hashCode()}.jpg")
					
					// 检查缩略图文件是否存在
					if (thumbnailFile.exists()) {
						onThumbnailGenerated(thumbnailFile.absolutePath) // 返回文件路径
					}
					
	                try {
	                    FileOutputStream(thumbnailFile).use { fos ->
	                        resource.compress(Bitmap.CompressFormat.JPEG, 90, fos)
	                        fos.flush()
	                    }
	                    onThumbnailGenerated(thumbnailFile.absolutePath) // 返回文件路径
	                } catch (e: Exception) {
	                    e.printStackTrace()
	                    onThumbnailGenerated(null)
	                }
	            }
	
	            override fun onLoadCleared(placeholder: Drawable?) {
	                // 清理资源时的回调
	            }
	
	            override fun onLoadFailed(errorDrawable: Drawable?) {
	                super.onLoadFailed(errorDrawable)
	                onThumbnailGenerated(null) // 加载失败返回 null
	            }
	        })
	}
	
	@kotlinx.coroutines.ExperimentalCoroutinesApi
	private suspend fun generateVideoThumbnailSync(videoPath: String): String? {
		return withContext(Dispatchers.IO) {
			val videoUri = Uri.parse("file://${videoPath}")
			suspendCancellableCoroutine<String?> { continuation ->
				Glide.with(context)
					.asBitmap()
					.load(videoUri)
					.frame(0) // 获取第一帧
					.into(object : CustomTarget<Bitmap>() {
						override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
							try {
								val cacheDir = context.cacheDir
								val thumbnailFile = File(cacheDir, "thumbnail_${videoUri.lastPathSegment.hashCode()}.jpg")
								
								// 如果文件已存在，直接返回路径
								if (thumbnailFile.exists()) {
									continuation.resume(thumbnailFile.absolutePath, { _ -> 
									
									})
									return
								}
								
								FileOutputStream(thumbnailFile).use { fos ->
									resource.compress(Bitmap.CompressFormat.JPEG, 90, fos)
									fos.flush()
								}
								continuation.resume(thumbnailFile.absolutePath, { _ ->
								 
								})
							} catch (e: Exception) {
								e.printStackTrace()
								// continuation.resumeWithException(e)
								if (continuation.isActive) {
									continuation.resumeWithException(e) // 改为使用 continuation.resumeWithException
								}
							}
						}

						override fun onLoadFailed(errorDrawable: Drawable?) {
							continuation.resume(null, { _ ->
							
							}) // 返回 null 表示失败
						}

						override fun onLoadCleared(placeholder: Drawable?) {
							// 不需要处理
						}
					})
			}
		}
	}
	
	// 获取视频第一帧
	private suspend fun generateThumbnailForVideo(videoUri: Uri): String? {
		return withContext(Dispatchers.IO) {
			val retriever = MediaMetadataRetriever()
			try {
				// 根据视频 URI 生成唯一的文件名
				val videoId = videoUri.lastPathSegment.hashCode().toString()
				val thumbnailFileName = "thumbnail_$videoId.jpg"
				val cacheDir = context.cacheDir
				val thumbnailFile = File(cacheDir, thumbnailFileName)
				
				// 检查缩略图文件是否存在
				if (thumbnailFile.exists()) {
					return@withContext thumbnailFile.absolutePath
				}
				
				retriever.setDataSource(context, videoUri)
				val bitmap = retriever.getFrameAtTime(0) // 获取视频的第一帧作为缩略图
	
				if (bitmap != null) {
					val fos = FileOutputStream(thumbnailFile)
					bitmap.compress(Bitmap.CompressFormat.JPEG, 90, fos)
					fos.close()
					thumbnailFile.absolutePath
				} else {
					null
				}
			} catch (e: Exception) {
				e.printStackTrace()
				null
			} finally {
				retriever.release()
			}
		}
	}
	
	// 获取视频长度
	fun getVideoDurationSync(videoPath: String): Long {
		val retriever = MediaMetadataRetriever()
		return try {
			retriever.setDataSource(videoPath)
			val duration = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION)
			duration?.toLong() ?: 0L // 返回时长，单位为毫秒
		} catch (e: Exception) {
			e.printStackTrace()
			0L
		} finally {
			retriever.release() // 释放资源
		}
	}
	
	fun getVideoDuration(videoPath: String, completion: (Long?) -> Unit) {
		CoroutineScope(Dispatchers.IO).launch {
			try {
				val duration = getVideoDurationWithContext(videoPath)
				withContext(Dispatchers.Main) {
					completion(duration)
				}
			} catch(e: Exception) {
				withContext(Dispatchers.Main) {
					completion(null)
				}
			}
		}
	}
	
	suspend fun getVideoDurationWithContext(videoPath: String): Long {
		return withContext(Dispatchers.IO) {
			val retriever = MediaMetadataRetriever()
			try {
				retriever.setDataSource(videoPath)
				val duration = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION)
				duration?.toLong() ?: 0L // 返回时长，单位为毫秒
			} catch (e: Exception) {
				e.printStackTrace()
				0L
			} finally {
				retriever.release() // 释放资源
			}
		}
	}
	
	// 获取本地音乐列表
	fun getLocalMusicList(): List<LocalMusic> {
	    val musicList = mutableListOf<LocalMusic>()
	    val musicUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
	    val selection = MediaStore.Audio.Media.IS_MUSIC + " != 0"
	    val projection = arrayOf(
	        MediaStore.Audio.Media._ID,
	        MediaStore.Audio.Media.TITLE,
	        MediaStore.Audio.Media.ARTIST,
	        MediaStore.Audio.Media.ALBUM,
	        MediaStore.Audio.Media.DATA,
	        MediaStore.Audio.Media.DURATION
	    )
	
	    val cursor: Cursor? = context.contentResolver.query(
	        musicUri,
	        projection,
	        selection,
	        null,
	        null
	    )
	
	    cursor?.use {
	        val idColumn = it.getColumnIndexOrThrow(MediaStore.Audio.Media._ID)
	        val titleColumn = it.getColumnIndexOrThrow(MediaStore.Audio.Media.TITLE)
	        val artistColumn = it.getColumnIndexOrThrow(MediaStore.Audio.Media.ARTIST)
	        val albumColumn = it.getColumnIndexOrThrow(MediaStore.Audio.Media.ALBUM)
	        val pathColumn = it.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA)
	        val durationColumn = it.getColumnIndexOrThrow(MediaStore.Audio.Media.DURATION)
	
	        while (it.moveToNext()) {
	            val id = it.getLong(idColumn)
	            val title = it.getString(titleColumn)
	            val artist = it.getString(artistColumn)
	            val album = it.getString(albumColumn)
	            val path = it.getString(pathColumn)
	            val duration = it.getLong(durationColumn)
				val contentUri = ContentUris.withAppendedId(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, id).toString()
	            musicList.add(LocalMusic(id, title, artist, album, path, duration, contentUri))
	        }
	    }
	
	    return musicList
	}
	
	// 根据相册分类名称获取该分类下的所有文件
	fun getMediaFilesByAlbumName(albumName: String): List<LocalMedia> {
		setLoadingData(true)
		try {
			val projection = arrayOf(
				MediaStore.Files.FileColumns._ID,
				MediaStore.Files.FileColumns.DATA,
				MediaStore.Files.FileColumns.MEDIA_TYPE
			)
			
			val selection = "${MediaStore.Files.FileColumns.MEDIA_TYPE} = ? OR " +
							"${MediaStore.Files.FileColumns.MEDIA_TYPE} = ? AND " +
			                "${MediaStore.Files.FileColumns.MEDIA_TYPE} = ?"
			val selectionArgs = arrayOf(
			    MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE.toString(),
			    MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO.toString(),
				albumName.lowercase()
			)
			val sortOrder = "${MediaStore.Files.FileColumns.DATE_ADDED} DESC"
			
			val uri: Uri = MediaStore.Files.getContentUri("external")
			
			val cursor: Cursor? = context.contentResolver.query(
				uri,
				projection,
				selection,
				selectionArgs,
				sortOrder
			)
			
			val mediaList = mutableListOf<LocalMedia>()
			
			cursor?.use {
				
			    val idColumn = it.getColumnIndexOrThrow(MediaStore.Files.FileColumns._ID)
			    val dataColumn = it.getColumnIndexOrThrow(MediaStore.Files.FileColumns.DATA)
			    val mediaTypeColumn = it.getColumnIndexOrThrow(MediaStore.Files.FileColumns.MEDIA_TYPE)
				
			    while (it.moveToNext()) {
			        val id = it.getLong(idColumn)
			        val dataPath = it.getString(dataColumn)
			        val mediaType = it.getInt(mediaTypeColumn) // 媒体类型：图片或视频
					val contentUri = ContentUris.withAppendedId(MediaStore.Files.getContentUri("external"), id)
			        mediaList.add(LocalMedia(id, dataPath, mediaType, contentUri.toString()))
			    }
			}
			
			return mediaList
		} finally {
			setLoadingData(false)
		}
	}

	// 获取相册数据
    fun loadAlbums(): List<Album> {
		setLoadingData(true)
		try {
			val projection = arrayOf(
			    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
			        MediaStore.Images.Media.BUCKET_DISPLAY_NAME
			    } else {
			        MediaStore.Images.Media.BUCKET_ID
			    },
				MediaStore.Files.FileColumns._ID,
				MediaStore.Files.FileColumns.DATA,
				MediaStore.Files.FileColumns.MEDIA_TYPE
			)
			val selection = "${MediaStore.Files.FileColumns.MEDIA_TYPE} = ? OR " +
							"${MediaStore.Files.FileColumns.MEDIA_TYPE} = ? AND " +
			                "${MediaStore.Files.FileColumns.MEDIA_TYPE} = ?"
			
			val selectionArgs = arrayOf(
			    MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE.toString(),
			    MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO.toString()
			)
			
			val sortOrder = "${MediaStore.Files.FileColumns.DATE_ADDED} DESC"
			
			val uri: Uri = MediaStore.Files.getContentUri("external")
			
			val cursor: Cursor? = context.contentResolver.query(
			    uri,
			    projection,
			    selection,
			    selectionArgs,
			    sortOrder
			)
			
			val albums = mutableListOf<Album>()
			val albumMap = mutableMapOf<String, MutableList<LocalMedia>>()
			
			cursor?.use {
			    val bucketDisplayNameIndex = it.getColumnIndexOrThrow(
			        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
			            MediaStore.Images.Media.BUCKET_DISPLAY_NAME
			        } else {
			            MediaStore.Images.Media.BUCKET_ID
			        }
			    )
				// val id = it.getColumnIndexOrThrow(MediaStore.Files.FileColumns.ID)
			 //    val dataIndex = it.getColumnIndexOrThrow(MediaStore.Files.FileColumns.DATA)
				val idColumn = it.getColumnIndexOrThrow(MediaStore.Files.FileColumns._ID)
				val dataColumn = it.getColumnIndexOrThrow(MediaStore.Files.FileColumns.DATA)
				val mediaTypeColumn = it.getColumnIndexOrThrow(MediaStore.Files.FileColumns.MEDIA_TYPE)
			
			    while (it.moveToNext()) {
					val id = it.getLong(idColumn)
			        val albumName = it.getString(bucketDisplayNameIndex)
			        val mediaPath = it.getString(dataColumn)
					val mediaType = it.getInt(mediaTypeColumn)
					val contentUri = ContentUris.withAppendedId(MediaStore.Files.getContentUri("external"), id)
			        albumMap.getOrPut(albumName) { mutableListOf() }.add(LocalMedia(id, mediaPath, mediaType, contentUri.toString()))
			    }
			}
			
			albumMap.forEach { (name, mediaUris) ->
			    albums.add(Album(name, mediaUris))
			}
			
			return albums
		} finally {
			setLoadingData(false)
		}
    }
}