/**
 * interface.uts
 * uts插件接口定义文件，按规范定义接口文件可以在HBuilderX中更好的做到语法提示
 */

/**
 * 错误码
 * 根据uni错误码规范要求，建议错误码以90开头，以下是错误码示例：
 * - 9010001 错误信息1
 * - 9010002 错误信息2
 */
export type KuxMediaManagerErrorCode = 9010001 | 9010002 | 9010003 | 9010004;
/**
 * myApi 的错误回调参数
 */
export interface KuxMediaManagerFail extends IUniError {
  errCode : KuxMediaManagerErrorCode
};

export type KuxLocalMedia = {
	// id
	id: number
	// 本地路径
	path: string
	contentUri?: string
	// 媒体类型 1-图片 2-视频
	mediaType: number
}

export type KuxMediaFileInfo = {
	// id
	id?: number
	// 展示名称
	displayName?: string
	// 图片被添加到媒体库的日期和时间。
	dateAdded?: number
	// 图片的最后修改日期和时间。
	dateModified?: number
	// 文件的MIME类型
	mimeType?: string
	// 文件的大小
	size?: number
	// 拍摄日期和时间，图片和视频类型文件有效
	dateTaken?: number
	// 图片的宽度，图片类型文件有效
	width?: number
	// 图片的高度，图片类型文件有效
	height?: number
	// 需要旋转的角度，图片类型文件有效
	orientation?: number
	// 拍摄时的维度，图片类型文件有效
	latitude?: number
	// 拍摄时的经度，图片类型文件有效
	longitude?: number
	// 缩略图路径
	thumbPath?: string
	// 所在的相册名称，图片和视频类型文件有效
	bucketDisplayName?: string
	// 所在的相册ID，图片和视频类型文件有效
	bucketId?: number
	// 时长，视频和音频文件有效
	duration?: number
	// 视频的分辨率，视频文件有效
	resolution?: string
	// 描述信息，图片和视频文件有效
	description?: string
	// 艺术家信息，视频和音频文件有效
	artist?: string
	// 专辑信息，视频和音频文件有效
	album?: string
	// 专辑的艺术家信息，音频文件有效
	albumArtist?: string
	// 作曲家，音频文件有效
	composer?: string
	// 年份，音频文件有效
	year?: number
	// 专辑的曲目编号，音频文件有效
	track?: number
	// 是否被设置为铃声，音频文件有效，0-否 1-是
	isRingtone?: number
	// 是否是音乐，音频文件有效，0-否 1-是
	isMusic?: number
	// 是否被设置为闹钟铃声，音频文件有效，0-否 1-是
	isAlarm?: number
	// 是否被设置为通知铃声，音频文件有效，0-否 1-是
	isNotification?: number
}

export type KuxAlbum = {
	name: string;
	medias: KuxLocalMedia[];
}

export type KuxLocalVideo = {
	id: number
	path: string
	contentUri?: string
	// 缩略图路径
	thumbPath?: string
	// 视频时长
	duration?: number
}

export type KuxLocalMusic = {
	id: number
	path: string
	contentUri?: string
	// 音乐标题
	title: string
	// 艺术家信息
	artist: string
	// 音乐专辑
	album: string
	// 音乐时长
	duration: number
}

export interface IKuxMediaManager {
	/**
	 * 是否正在加载媒体资源
	 */
	isLoading(): boolean;
	/**
	 * 获取相册分类名列表
	 */
	getAlbumCategories(): string[];
	/**
	 * 获取指定分类的媒体数量
	 */
	getMediaFileCountForAlbum(name: string): number;
	/**
	 * 获取最近媒体数据
	 */
	getRecentMedia(): KuxLocalMedia[];
	/**
	 * 获取相册所有媒体
	 */
	loadAlbums(): KuxAlbum[];
	/**
	 * 获取指定分类下的媒体数据
	 */
	getMediaFilesByAlbumName(name: string): KuxLocalMedia[];
	/**
	 * 获取本地视频
	 */
	getLocalVideos(): KuxLocalVideo[];
	/**
	 * 生成视频缩略图
	 */
	generateVideoThumbnail(videoPath: string, completion: (thumbPath: string | null) => void): void;
	/**
	 * 生成视频缩略图【同步版本】
	 */
	generateVideoThumbnailSync(videoPath: string): Promise<string | null>;
	/**
	 * 获取视频时长
	 */
	getVideoDuration(videoPath: string, completion: (duration: number | null) => void): void;
	/**
	 * 获取视频时长【同步版本】
	 */
	getVideoDurationSync(videoPath: string): Promise<number | null>;
	/**
	 * 获取本地音乐列表
	 */
	getLocalMusicList(): KuxLocalMusic[];
	/**
	 * 获取指定路径的媒体文件信息
	 */
	getMediaFileInfo(fileId: number, completion: (fileInfo: KuxMediaFileInfo) => void): void;
	/**
	 * 获取指定路径的媒体文件信息【同步版本】
	 */
	getMediaFileInfoSync(fileId: number): Promise<KuxMediaFileInfo>;
	/**
	 * 捕获全局错误
	 */
	onError(callback: (error: IUniError) => void): void;
}